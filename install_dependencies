#!/bin/bash

if [ "$EUID" -ne 0 ]; then
  echo "Please run as root."
  echo "Like this > sudo ./install_dependencies"
  exit
fi

echo "Running updating package manager lists..."

apt update -qq

echo "Installing useful utilities..."

apt install -y \
  mesa-utils

echo "Installing FFmpeg dependencies..."

apt -y install \
  autoconf \
  automake \
  build-essential \
  cmake \
  git-core \
  libass-dev \
  libfreetype6-dev \
  libgnutls28-dev \
  libmp3lame-dev \
  libsdl2-dev \
  libtool \
  libva-dev \
  libvdpau-dev \
  libvorbis-dev \
  libxcb1-dev \
  libxcb-shm0-dev \
  libxcb-xfixes0-dev \
  meson \
  ninja-build \
  pkg-config \
  texinfo \
  wget \
  yasm \
  zlib1g-dev

echo "Installing Ubuntu 20.04 specific dependencies..."

apt install -y \
  libunistring-dev \
  libaom-dev


echo "Installing additional FFmpeg dependencies..."

apt install -y \
  nasm \
  libx264-dev \
  libx265-dev \
  libnuma-dev \
  libvpx-dev \
  libfdk-aac-dev \
  libopus-dev

echo "Installing Shadertoy dependencies..."

apt install -y \
  libglfw3 \
  libglfw3-dev \
  libglew-dev

# Ubuntu jammy has libglew 2.2, but focal has libglew 2.1
JAMMY=$(cat /etc/os-release | grep UBUNTU_CODENAME=jammy)
if [ ${JAMMY} ]; then
  apt install libglew2.2
else
  apt install libglew2.1
fi
