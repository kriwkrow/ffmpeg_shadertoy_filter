# FFmpeg Shadertoy Filter

A [FFmpeg](https://ffmpeg.org/) filter to apply [Shadertoy](https://www.shadertoy.com/) GLSL code to your videos.

![Example using Fast Liquid Research shader](images/FastLiquidResearch.jpg)

Based on these awesome previous projects:

- https://gitlab.com/Canta/ffmpeg_shadertoy_filter
- https://github.com/transitive-bullshit/ffmpeg-gl-transition
- https://github.com/nervous-systems/ffmpeg-opengl
- https://github.com/numberwolf/FFmpeg-Plus-OpenGL

Still a work in progress. Currently works with single-input, image-only shadertoys.

## Installation

You **NEED** to compile FFmpeg. You **CAN'T** use this filter without compiling FFmpeg yourself. There are a few scripts here to make your life a bit easier.

1. Open up Terminal using key combination `CTRL` + `ALT` + `T` and change directory to Desktop.
    ```
    cd ~/Desktop
    ```

1. Make sure your system has [Git](https://git-scm.com/) version control system installed.
    ```
    sudo apt install -y git
    ```

1. Clone this repository. Downloading a zip is fine too.
    ```
    git clone --depth=1 https://gitlab.com/kriwkrow/ffmpeg_shadertoy_filter.git
    ```

1. Navigate into it using Terminal or use the "right-click" -> "Open in Terminal" option.
    ```
    cd ffmpeg_shadertoy_filter
    ```

1. Install dependencies. This has to be run as root (`sudo`).
    ```
    sudo ./install_dependencies
    ```

1. Download and unzip FFmpeg source files. This will create a directory `ffmpeg_source`.
    ```
    git clone --depth=1 https://git.ffmpeg.org/ffmpeg.git ffmpeg_source
    ```

1. Patch FFmpeg source with changes needed for the Shadertoy filter. The script copies `vf_shadertoy.c` where it should be in FFmpeg source as well as injects a few lines in FFmpeg configuration files.
    ```
    ./patch_ffmpeg
    ```

1. Configure FFmpeg for your system. It will set current directory as the destination for final binary files.
    ```
    ./configure_ffmpeg
    ```

1. Compile and install FFmpeg. This should create three new files (`ffmpeg`, `ffplay` and `ffprobe`) as well as a directory `ffmpeg_build`.
    ```
    ./compile_ffmpeg
    ```

## Usage:

You can use the `example/input.mp4` and `example/shader.glsl` located in this repository to test if it works. This should create a video `output.mp4`. You can compare it with the `example/expected.mp4` video to see how it should work. This assumes that you are `ffmpeg` compiled and installed in the repository directory. If you have not compiled it yet, follow [installation instructions](#installation-instructions) below.

```
./ffmpeg -i example/input.mp4 -vf "shadertoy=example/shader.glsl" -c:v libx264 -preset slow -crf 25 -f mp4 -c:a copy output.mp4
```

It could also be used by `ffplay`, like this.

```
ffplay example/input.mp4 -vf "shadertoy=example/shader.glsl" -fs -loop 0
```

## Notes

- So far, this filter works wonderful with effects shadertoys (like night vision, vcr artifacts, and stuff like that).
- Some shadertoys need minor tweaking because otherwise the output get vertically flipped.
- Many (of the coolest) shadertoys use multiple inputs and/or multiple buffers. The filter don't have that yet. I want to add the ability to have multiple video inputs, but have little time for it, and no ETA. Second in priority is the ability to use multiple buffers, and last in line is audio.
- Shadertoy website uses default noises that we don't provide here. So you may have to get your hands dirty to obtain the same results from some shadertoys, specially the ones that consist in amazing animations. So try at first rendering with different video files, and eventually just go for lavfi wizardry.
- Some shadertoys may just plain not work, and you'll have to understand their code in order to fix them. *C'est la vie*.

## Licence

I do libre software, so IYAM just use it. But consider this software:

- Has been made using bits of code from other previous software. Mostly the repos mentioned before, and shadertoy website.
- Shadertoys code may each one have their own licensing.
- IDK what's the deal with FFMPEG license when it's about filters.
- The same goes for the GL libs.

So, frankly, I have no idea. But if it depends on me, then the license I would choose is GPLv3.

## Disclaimer about compatibility

I'm a GNU/Linux user, and this works on GNU/Linux.

I'm neither user, friend, enthusiast, evangelist, customer, partner, employee, or anything other than enemy of Microsoft and their products, including Windows. So I could not care less about how to make this work on that system. Consider this repo explicitly hostile towards Microsoft. Same goes for Apple stuff: I don't use that, I see no reason at all to use that, and I see that stuff hurting people's rights all around the world, so IMHO should be avoided like a disease. If you want this working on those systems, just fork it and do with the code as you please.
